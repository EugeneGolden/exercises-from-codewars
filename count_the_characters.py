"""
Description:

The goal of this kata is to write a function that takes two inputs: a string and a character. The function will count the number of times that character appears in the string.
The count is case insensitive.

For example:

count_char("fizzbuzz","z") => 4
count_char("Fancy fifth fly aloof","f") => 5

The character can be any alphanumeric character.
"""

def count_char(phrase, char):
    phrase = phrase.lower()
    char = char.lower()
    i = 0
    for ch in phrase:
        if ch == char:
            i += 1
    return i

count_char('Hello there', 'L')

def count_char2(s, c):
    s = s.lower()
    c = c.lower()
    return s.count(c)
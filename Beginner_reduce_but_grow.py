"""
Given and array of integers (x), return the result of multiplying the values together in order. Example:

[1, 2, 3] --> 6

For the beginner, try to use the reduce method - it comes in very handy quite a lot so is a good one to know.

Array will not be empty.

"""

def grow(arr):
    mult = 1
    for x in arr:
        mult = mult * x
    return mult

"""
def grow(arr):
    return reduce(lambda x, y: x * y, arr)
"""
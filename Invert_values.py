"""
Invert a given list of integer values.

Python/JS:

invert([1,2,3,4,5]) == [-1,-2,-3,-4,-5]
invert([1,-2,3,-4,5]) == [-1,2,-3,4,-5]
invert([]) == []

You can assume that all values are integers.

"""

def invert(lst):
    invert_lst = []
    for i in lst:
        invert_lst.append(-i)
    return invert_lst


def invert(lst):
    return [a * -1 for a in lst]

invert([1,2,3,4,5])

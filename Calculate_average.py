"""
Write function avg which calaculates average of numbers in given list.
"""

def find_average(array):
    sum = 0
    if len(array) == 0:
        return 0
    else:
        for i in array:
            sum += i
        return (sum / len(array))

"""
def find_average(array):
    return 0 if len(array) == 0 else sum(array) / len(array)
"""
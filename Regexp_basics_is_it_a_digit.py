#import re

"""
Implement String#digit? (in Java StringUtils.isDigit(String)),
which should return true if given object is a digit (0-9), false otherwise.


def is_digit(n):
    return True if re.match('\d', n) else False

    the wrong decision
    """

import re
def is_digit(n):
    if len(n) == 1 and re.match(r'^\d$', n):
        return True
    return False
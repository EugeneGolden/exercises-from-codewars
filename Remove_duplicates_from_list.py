"""
Write function distinct that removes duplicate from list of numbers.

The order of the sequence needs to stay the same.
"""

def distinct(seq):
    new_seq = []
    for elem in seq:
        if elem in new_seq:
            continue
        else:
            new_seq.append(elem)
    return new_seq

"""
def distinct(seq):
    nl = []
    [nl.append(i) for i in seq if i not in nl]
    return nl
"""
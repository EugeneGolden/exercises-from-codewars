import math
"""
from math import sqrt
def square_or_square_root(arr):
    return [sqrt(n) if sqrt(n).is_integer() else n*n for n in arr]

from math import sqrt

def square_or_square_root(arr):
    return map(lambda x: int(sqrt(x)) if int(sqrt(x))**2 == x else x**2, arr)
"""
def square_or_square_root(arr):
    result = []
    for elem in arr:
        if int(math.sqrt(elem)) ** 2 == elem:
            result.append(math.sqrt(elem))
        else:
            result.append(elem ** 2)
    return result

# coding=utf-8
import datetime
import os
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
import time

buttonVoiti = "//button[@id='index_login_button']"
countPersons = "//span[@class='header_count fl_l']"
emailField = "//input[@id='index_email']"
emailValue = ""
fileNamePrevious = 'C:\Users\John\PycharmProjects\Exercises\Exercises 2017-3-12 18h 52m 32s people_list.txt'
menuBunker = "//a[@href='/bunkergym']"
passwordField = "//input[@id='index_pass']"
passwordValue = ""
urlBunkergym = "https://vk.com/bunkergym"
urlBunkerUsers = "https://vk.com/bunkergym?act=users"
vkUrl = "https://vk.com/"
xpathPerson = "//a[@class='group_u_title']"

# Create a new instance of the Firefox driver
driver = webdriver.Firefox()

# go to vk homepage
driver.get(vkUrl)

# find e-mail input
email = driver.find_element_by_xpath(emailField)
email.send_keys(emailValue)

# find password field
password = driver.find_element_by_xpath(passwordField)
password.send_keys(passwordValue)

# find enter button
voiti = driver.find_element_by_xpath(buttonVoiti)
voiti.submit()
# wait for the item in the left menu
WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, menuBunker)))

# go directly to BunkerGYM page in vk
driver.get(urlBunkergym)

# gym = driver.find_element_by_xpath(menuBunker)
# gym.click()
# "//span[@class='left_label inl_bl'][text()='Тренажерный зал..']"

# examine amount of participants of the group
count = driver.find_element_by_xpath(countPersons)
amount = int(count.text)

# go directly to the list of the participants
driver.get(urlBunkerUsers)

# examine the current participants who are visible
persons = driver.find_elements_by_xpath(xpathPerson)

# make the whole page visible scrolling down the page
while len(persons) < amount:
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(1)
    persons = driver.find_elements_by_xpath(xpathPerson)

# examine the list of the participants again
persons = driver.find_elements_by_xpath(xpathPerson)

# get a list with participants' names
def getPeopleName(persons):
    peopleList = []
    for i in persons:
        if i.text != '':
            peopleList.append((i.text).encode("utf-8"))
    return peopleList

# get list with participants of the group
listNew = getPeopleName(persons)

# write results in files
def log(data, fileName):
    if isinstance(data, list):
        logFile = open(fileName, 'w+')
        for m in data:
            logFile.write(m + '\n')
        logFile.close()
    elif isinstance(data, dict):
        logFile = open(fileName, 'w+')
        for k, v in data.items():
            logFile.write(k + ' ' + v + '\n')
        logFile.close()

def addTracingZero(value):
    text = str(value)
    return text if (len(text) >= 2) else ("0" + text)

def generateFolderName():
    dt = datetime.datetime.now()
    currentTime = (" %s-%s-%s %sh %sm %ss" % (dt.year, dt.month, dt.day,
                                              addTracingZero(dt.hour), addTracingZero(dt.minute),
                                              addTracingZero(dt.second)))
    return currentTime

# get current folder name
currentFolder = os.getcwd()

# generate a new folder name which is like "2017-3-12 01h 09m 44s people_list"
folder = generateFolderName()

# put that generated folder inside the current one
newFolder = currentFolder + folder

# created a file inside that folder
fileParticipants = newFolder + ' people_list.txt'

#and one more for the comparison
fileComparison = newFolder + ' addedLeft.txt'

# insert participants' names inside the file
logging = log(listNew, fileParticipants)

# open "old" file
filePrevious = open(fileNamePrevious, 'r')

# create a list for participants from the old file
listPrevious = []

for line in filePrevious:
    listPrevious.append(line.strip(' ').strip('\n'))
    print line

# create a dictionary for comparison result:
# we expaect to have records like "Added_1 Ivanov Ivan , Left_1 Petrov Petr"
dic = {}

def compare(listNew, listPrevious):
    i = 1
    for person in listNew:
        if person not in listPrevious:
            print person
            dic['Added_%d' % i] = person
            i += 1
    i = 1
    for person in listPrevious:
        if person not in listNew:
            print 'person ' + person
            dic['Left_%d' % i] = person
            i += 1
    return dic

# compare lists with old participants and new ones
comparison = compare(listNew, listPrevious)

# write results in files
logging_comparison = log(comparison, fileComparison)

driver.quit()












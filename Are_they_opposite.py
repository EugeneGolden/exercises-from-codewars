"""
assume that the string only contains letters or it's a empty string

    Some examples:

isOpposite("ab","AB") should return true;
isOpposite("aB","Ab") should return true;
isOpposite("aBcd","AbCD") should return true;
isOpposite("AB","Ab") should return false;
isOpposite("","") should return false;
"""

def is_opposite(s1,s2):
    result = 0
    if s1.isalpha():
        for i in range(len(s1)):
            if s1[i].islower() and s2[i].isupper():
                return True
            elif s1[i].isupper() and s2[i].islower():
                return True
            else:
                result += 1
                break
    else:
        result += 1
    return True if result == 0 else False

"""
def is_opposite(s1, s2):
    return False if not(s1 or s2) else s1.swapcase() == s2
"""
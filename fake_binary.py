"""
Given a string of digits, you should replace any digit below 5 with '0' and any digit 5 and above with '1'. Return the resulting string.
"""

def fake_bin(x):
    list1 = []
    for i in range(len(x)):
        if int(x[i]) < 5:
            list1.append('0')
        else:
            list1.append('1')
    return "".join(list1)

"""
def fake_bin(x):
    return ''.join('0' if c < '5' else '1' for c in x)
"""
"""
Write function makeParts that will take an array as argument and the size of the chunk.

Example: if an array of size 123 is given and chunk size is 10 there will be 13 parts, 12 of size 10 and 1 of size 3.

"""
"""

def makeParts(arr, csize):
  return [ arr[i: i + csize] for i in range(0, len(arr), csize)]
"""

def makeParts(arr, chunkSize):
    r = list()
    while arr:
        r.append(arr[0:chunkSize])
        arr = arr[chunkSize:]
    return r

makeParts([1,2,3,4,5], 2)
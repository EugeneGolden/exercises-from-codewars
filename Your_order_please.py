"""
Description:

Your task is to sort a given string. Each word in the String will contain a single number. This number is the position the word should have in the result.

Note: Numbers can be from 1 to 9. So 1 will be the first word (not 0).

If the input String is empty, return an empty String. The words in the input String will only contain valid consecutive numbers.

For an input: "is2 Thi1s T4est 3a" the function should return "Thi1s is2 3a T4est"
"""

def order(sentence):
    split_sent = sentence.split()
    sorted_list = []
    i = 1
    space = " "
    for round in range(0, len(split_sent)):
        for word in split_sent:
            for character in word:
                try:
                    if int(character) == i:
                        sorted_list.append(word)
                        i += 1
                        if len(split_sent) == len(sorted_list):
                            break
                        continue
                except ValueError:
                    continue
    if len(split_sent) == 0:
        return ''
    else:
        return space.join(sorted_list)

# order("is2 Thi1s T4est 3a")
order("")


"""
The solution which I liked:

def order(sentence):
  if sentence == "":
      return sentence

  output = []

  for ordernum in range(1, len(sentence.split()) + 1):
      for word in sentence.split():
          for letter in word:
              if letter == str(ordernum):
                  output.append(word)

  return " ".join(output)
"""
def group(arr):
    result_lst = []
    #calculate how many unique items in arr
    length = []
    for elem in arr:
        if elem not in length:
            length.append(elem)
    #generate a dictionary which contains the necessary amount of lists
    #which we're going to use
    dct = {}
    for elem in length:
        dct['lst_%s' % elem] = []
    #let's fill all of those lists
    for elem in length:
        amount = arr.count(elem)
        for i in range(amount):
            dct['lst_%s' % elem].append(elem)
        result_lst.append(dct['lst_%s' % elem])
    return result_lst

"""
def group(A):
    result, seen = [], []
    for element in A:
        if element not in seen:
            seen.append(element)
            result.append([element] * A.count(element))
    return result
"""

group([3, 2, 6, 2, 1, 3])
"""
Description:

Given two integers, which can be positive and negative, find the sum of all the numbers between including them too and return it. If both numbers are equal return a or b.

Note! a and b are not ordered!

Example:
get_sum(1, 0) == 1   // 1 + 0 = 1
get_sum(1, 2) == 3   // 1 + 2 = 3
get_sum(0, 1) == 1   // 0 + 1 = 1
get_sum(1, 1) == 1   // 1 Since both are same
get_sum(-1, 0) == -1 // -1 + 0 = -1
get_sum(-1, 2) == 2  // -1 + 0 + 1 + 2 = 2
"""

def get_sum(a,b):
    list_sorted = sorted([a, b])
    element = list_sorted[0]
    sum = 0
    if a == b:
        return a
    else:
        while (element <= list_sorted[1]):
            sum = sum + element
            element += 1
        return sum

get_sum(2, -1)
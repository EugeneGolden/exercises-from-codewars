"""
We all use 16:9, 16:10, 4:3 etc. ratios every day. Main task is to determine image ratio by its width and height dimensions.

Function should take width and height of an image and return a ratio string (ex."16:9"). If any of width or height entry is 0 function should throw an exception (or return Nothing).

"""
def calculate_ratio(w,h):
    counter = 0
    if w == 0 or h == 0:
        return "You threw an error"
    if type(w) == float:
        return "{}:{}".format(w/h,h/h)
    for i in range(2,h+1):
        if w % i == 0 and h % i == 0:
            counter = i
    if counter == 0 and w != 0 and h !=0:
        return "{}:{}".format(w,h)
    return "{}:{}".format(w/counter,h/counter)

# didn't do it by myself
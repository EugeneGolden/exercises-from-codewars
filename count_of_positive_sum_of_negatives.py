"""
Description:

Given an array of integers.

Return an array, where the first element is the count of positives numbers and the second element is sum of negative numbers.
"""

def count_positives_sum_negatives(arr):
    result = []
    amount = 0
    sum_negatives = 0
    if arr == [] or arr == None:
        return result
    for i in arr:
        if i > 0:
            amount += 1
        else:
            sum_negatives += i
    result.append(amount)
    result.append(sum_negatives)
    return result

"""
def count_positives_sum_negatives(arr):
    if not arr: return []
    pos = 0
    neg = 0
    for x in arr:
      if x > 0:
          pos += 1
      if x < 0:
          neg += x
    return [pos, neg]
---------------------------------------------
def count_positives_sum_negatives(arr):
    pos = sum(1 for x in arr if x > 0)
    neg = sum(x for x in arr if x < 0)
    return [pos, neg] if len(arr) else []
"""

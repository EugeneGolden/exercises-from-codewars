"""
Complete the squareSum method so that it squares each number passed into it and then sums the results together.

For example:

square_sum([1, 2, 2]) # should return 9
"""


def square_sum(numbers):
    sum = 0
    for elem in numbers:
        sum = sum + (elem ** 2)
    return sum

square_sum([1,2])

"""
def square_sum(numbers):
    return sum(x ** 2 for x in numbers)
"""
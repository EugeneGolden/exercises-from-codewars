"""
Write a function which removes from string all non-digit characters and parse the remaining to number. E.g: "hell5o wor6ld" -> 56
"""

def get_number_from_string(s):
    digits = ''
    for elem in s:
        if elem.isdigit():
            digits = digits + elem
    return int(digits)

"""
def get_number_from_string(strng):
    return int(''.join(a for a in strng if a.isdigit()))
"""
"""
altERnaTIng cAsE <=> ALTerNAtiNG CaSe

Define to_alternating_case(char*) such that each lowercase letter becomes uppercase and each uppercase letter becomes lowercase. For example:
"""

def to_alternating_case(strin):
    return strin.swapcase()